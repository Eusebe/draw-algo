package org.eeesebio.draw.algorithm.service;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.eeesebio.draw.algorithm.exception.NoSolutionException;
import org.eeesebio.draw.algorithm.model.NotAllowedConstraint;
import org.jacop.core.Domain;
import org.jacop.core.SmallDenseDomain;
import org.junit.Test;

public class DrawServiceTest {

	private static final int A_BIG_AMOUNT_OF_PERSONS = 4;
	private Domain[] solution;
	private ArrayList<NotAllowedConstraint> constraints = new ArrayList<NotAllowedConstraint>();;
	DrawService draw = new DrawService();

	@Test
	public void testGetSingleSolution() throws NoSolutionException {

		constraints.add(new NotAllowedConstraint(0, 0));
		constraints.add(new NotAllowedConstraint(1, 1));
		constraints.add(new NotAllowedConstraint(2, 2));
		constraints.add(new NotAllowedConstraint(0, 1));

		// 3 elements to distribute
		solution = draw.getSolution(3, constraints);

		assertThat(((SmallDenseDomain) solution[0]).min, is(2));
		assertThat(((SmallDenseDomain) solution[1]).min, is(0));
		assertThat(((SmallDenseDomain) solution[2]).min, is(1));
	}

	@Test(expected = NoSolutionException.class)
	public void throws_an_exception_when_no_solution_exist() throws NoSolutionException {

		constraints.add(new NotAllowedConstraint(0, 0));
		constraints.add(new NotAllowedConstraint(0, 1));
		constraints.add(new NotAllowedConstraint(0, 2));

		// 3 elements to distribute
		solution = draw.getSolution(3, constraints);
	}

	@Test
	public void prevents_from_assigning_several_times_a_single_item() throws NoSolutionException {
		solution = draw.getSolution(A_BIG_AMOUNT_OF_PERSONS, constraints);
		for (int i = 0; i < solution.length; i++) {
			int assigneeIndex = ((SmallDenseDomain) solution[i]).min;
			for (int j = i + 1; j < solution.length; j++) {
				int assignedIndex = ((SmallDenseDomain) solution[j]).min;
				assertNotEquals("item " + i + " got assignment #" + assigneeIndex + " ; item " + j + " got assignment #" + assignedIndex , assigneeIndex, assignedIndex);
			}
		}
	}
}
