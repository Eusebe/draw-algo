package org.eeesebio.draw.algorithm.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotAllowedConstraint {
	@XmlElement
	private int owner;
	@XmlElement
	private int notToBeAssociated;

	public NotAllowedConstraint() {
	}
	
	public NotAllowedConstraint(int owner, int notToBeAssociated) {
		this.owner = owner;
		this.notToBeAssociated = notToBeAssociated;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public int getNotToBeAssociated() {
		return notToBeAssociated;
	}

	public void setNotToBeAssociated(int notToBeAssociated) {
		this.notToBeAssociated = notToBeAssociated;
	}
}
