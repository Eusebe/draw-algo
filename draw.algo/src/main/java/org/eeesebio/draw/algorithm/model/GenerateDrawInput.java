package org.eeesebio.draw.algorithm.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GenerateDrawInput {

	@XmlElement
	private Integer size;
	
	@XmlElement
	private List<NotAllowedConstraint> constraints = new ArrayList<NotAllowedConstraint>(); 

	public GenerateDrawInput() {
	}
	
	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public List<NotAllowedConstraint> getConstraints() {
		return constraints;
	}

	public void setConstraints(List<NotAllowedConstraint> constraints) {
		this.constraints = constraints;
	}
}
