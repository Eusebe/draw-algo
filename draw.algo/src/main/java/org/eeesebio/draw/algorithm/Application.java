package org.eeesebio.draw.algorithm;

import java.util.Collection;

import javax.inject.Inject;
import javax.ws.rs.POST;

import org.eeesebio.draw.algorithm.exception.NoSolutionException;
import org.eeesebio.draw.algorithm.model.DrawResult;
import org.eeesebio.draw.algorithm.model.GenerateDrawInput;
import org.eeesebio.draw.algorithm.model.NotAllowedConstraint;
import org.eeesebio.draw.algorithm.service.DrawService;
import org.jacop.core.Domain;
import org.jacop.core.SmallDenseDomain;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Application extends SpringBootServletInitializer {

	@Inject
	private DrawService drawService;

	@RequestMapping("/hi")
	public String sayHello() {
		return "hello world ! (at least the spring boot one)";
	}

	@POST
	@RequestMapping("/generate")
	public DrawResult generateDraw(GenerateDrawInput input) throws NoSolutionException {
		Collection<NotAllowedConstraint> constraints = input.getConstraints();

		Domain[] solution = drawService.getSolution(input.getSize(), constraints);

		DrawResult drawResult = populateDrawResult(solution);

		return drawResult;
	}

	private DrawResult populateDrawResult(Domain[] solution) {
		DrawResult drawResult = new DrawResult();
		for (int j = 0; j < solution.length; j++) {
			drawResult.getParticipants().put(Integer.toString(j),
					Integer.toString(((SmallDenseDomain) solution[j]).min));
		}
		return drawResult;
	}

	// for standalone
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	// for deployment in a servlet container
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
}
