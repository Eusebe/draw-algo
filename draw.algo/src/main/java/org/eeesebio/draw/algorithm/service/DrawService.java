package org.eeesebio.draw.algorithm.service;

import java.util.Collection;
import java.util.Random;

import org.eeesebio.draw.algorithm.exception.NoSolutionException;
import org.eeesebio.draw.algorithm.model.NotAllowedConstraint;
import org.jacop.constraints.Alldifferent;
import org.jacop.constraints.Among;
import org.jacop.core.Domain;
import org.jacop.core.IntVar;
import org.jacop.core.IntervalDomain;
import org.jacop.core.SmallDenseDomain;
import org.jacop.core.Store;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.IndomainMin;
import org.jacop.search.InputOrderSelect;
import org.jacop.search.Search;
import org.jacop.search.SelectChoicePoint;
import org.springframework.stereotype.Component;

@Component
public class DrawService {

	public Domain[] getSolution(int size, Collection<NotAllowedConstraint> notAllowed) throws NoSolutionException {
		Store store = new Store();
		IntVar[] vars = initVariables(store, size);

		// impose qu'une personne ne soit pas affectee 2 fois
		store.impose(new Alldifferent(vars));

		// ajoute les contraintes "notAllowed"
		for (NotAllowedConstraint constraint : notAllowed) {
			addNotAllowedConstraint(store, size, vars[constraint.getOwner()], constraint.getNotToBeAssociated());
		}

		Search<IntVar> search = launchSearch(store, vars);

		checkAnySolutionExists(search);

		return search.getSolution(getRandomSolutionIndex(search));

		// displaySolution(solution);
	}

	private Search<IntVar> launchSearch(Store store, IntVar[] vars) {
		Search<IntVar> search = new DepthFirstSearch<IntVar>();
		SelectChoicePoint<IntVar> select = initSearchAndGetSelectChoicePoint(store, vars, search);
		// effectue la recherche
		search.labeling(store, select);
		return search;
	}

	private SelectChoicePoint<IntVar> initSearchAndGetSelectChoicePoint(Store store, IntVar[] vars, Search<IntVar> search) {
		search.getSolutionListener().searchAll(true);
		search.getSolutionListener().recordSolutions(true);
		SelectChoicePoint<IntVar> select = new InputOrderSelect<IntVar>(store, vars, new IndomainMin<IntVar>());
		return select;
	}

	private int getRandomSolutionIndex(Search<IntVar> search) {
		// first index is 1
		return new Random().nextInt(search.getSolutionListener().solutionsNo()) + 1;
	}

	private void checkAnySolutionExists(Search<IntVar> search) throws NoSolutionException {
		if (search.getSolutionListener().solutionsNo() == 0) {
			throw new NoSolutionException();
		}
	}

	private IntVar[] initVariables(Store store, int size) {
		IntVar[] vars = new IntVar[size];
		for (int i = 0; i < size; i++) {
			vars[i] = new IntVar(store, Integer.toString(i), 0, size - 1);
		}
		return vars;
	}

	// private static void displaySolutions(Search<IntVar> search, int
	// randomIndex) {
	// for (int i = 1; i <= search.getSolutionListener().solutionsNo(); i++) {
	// if (randomIndex == i) {
	// System.out.print("-> ");
	// }
	// System.out.print("\tSolution " + i + ": ");
	// displaySolution(search.getSolution(i));
	// }
	// }

	public void displaySolution(Domain[] solution) {
		for (int j = 0; j < solution.length; j++) {
			System.out.print(((SmallDenseDomain) solution[j]).min + " ");
		}
		System.out.println();
	}

	private void addNotAllowedConstraint(Store store, int participantsSize, IntVar var, int blocked) {
		IntervalDomain kSet = new IntervalDomain();
		for (int i = 0; i < participantsSize; i++) {
			if (i != blocked) {
				kSet.addDom(new IntervalDomain(i, i));
			}
		}
		store.impose(new Among(new IntVar[] { var }, kSet, new IntVar(store, 1, 1)));
	}
}
