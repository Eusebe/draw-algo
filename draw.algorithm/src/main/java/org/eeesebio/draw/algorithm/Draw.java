package org.eeesebio.draw.algorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import org.eeesebio.draw.algorithm.model.NotAllowedConstraint;
import org.jacop.constraints.Alldifferent;
import org.jacop.constraints.Among;
import org.jacop.core.Domain;
import org.jacop.core.IntVar;
import org.jacop.core.IntervalDomain;
import org.jacop.core.SmallDenseDomain;
import org.jacop.core.Store;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.IndomainMin;
import org.jacop.search.InputOrderSelect;
import org.jacop.search.Search;
import org.jacop.search.SelectChoicePoint;

public class Draw {

	private Domain[] solution;
	
	public Draw(int size, Collection<NotAllowedConstraint> notAllowed) {
		Store store = new Store();
		IntVar[] vars = initVariables(store, size);

		// impose qu'une personne ne soit pas affect�e 2 fois
		store.impose(new Alldifferent(vars));

		// ajoute les contraintes "notAllowed"
		for (NotAllowedConstraint constraint : notAllowed) {
			addNotAllowedConstraint(store, size, vars[constraint.getOwner()], constraint.getNotToBeAssociated());
		}

		Search<IntVar> search = new DepthFirstSearch<IntVar>();
		search.getSolutionListener().searchAll(true);
		search.getSolutionListener().recordSolutions(true);
		SelectChoicePoint<IntVar> select = new InputOrderSelect<IntVar>(store, vars, new IndomainMin<IntVar>());
		// effectue la recherche
		search.labeling(store, select);
		// first index is 1
		int randomIndex = new Random().nextInt(search.getSolutionListener().solutionsNo()) + 1;

		setSolution(search.getSolution(randomIndex));
		
		
//		displaySolution(solution);

	}

	private IntVar[] initVariables(Store store, int size) {
		IntVar[] vars = new IntVar[size];
		for (int i = 0; i < size; i++) {
			vars[i] = new IntVar(store, Integer.toString(i), 0, size - 1);
		}
		return vars;
	}

//	private static void displaySolutions(Search<IntVar> search, int randomIndex) {
//		for (int i = 1; i <= search.getSolutionListener().solutionsNo(); i++) {
//			if (randomIndex == i) {
//				System.out.print("-> ");
//			}
//			System.out.print("\tSolution " + i + ": ");
//			displaySolution(search.getSolution(i));
//		}
//	}

	private static void displaySolution(Domain[] solution) {
		for (int j = 0; j < solution.length; j++) {
			System.out.print(((SmallDenseDomain) solution[j]).min + " ");
		}
		System.out.println();
	}

	private void addNotAllowedConstraint(Store store, int participantsSize, IntVar var, int blocked) {
		IntervalDomain kSet = new IntervalDomain();
		for (int i = 0; i < participantsSize; i++) {
			if (i != blocked) {
				kSet.addDom(new IntervalDomain(i, i));
			}
		}
		store.impose(new Among(new IntVar[] { var }, kSet, new IntVar(store, 1, 1)));
	}

	public static void main(String[] args) {

		ArrayList<NotAllowedConstraint> constraints = new ArrayList<NotAllowedConstraint>();
		constraints.add(new NotAllowedConstraint(0, 0));
		constraints.add(new NotAllowedConstraint(1, 1));
		constraints.add(new NotAllowedConstraint(2, 2));
		constraints.add(new NotAllowedConstraint(0, 1));
		constraints.add(new NotAllowedConstraint(0, 2));

		// 3 elements to distribute
		Draw draw = new Draw(3, constraints);
		displaySolution(draw.getSolution());
	}

	public Domain[] getSolution() {
		return solution;
	}

	private void setSolution(Domain[] solution) {
		this.solution = solution;
	}
}
