package org.eeesebio.draw.algorithm.model;

import java.util.HashMap;

public class DrawResult {

	private HashMap<String, String> participants;

	public DrawResult() {
		participants = new HashMap<String, String>();
	}

	public HashMap<String, String> getParticipants() {
		return participants;
	}

	public void setParticipants(HashMap<String, String> participants) {
		this.participants = participants;
	}
}
