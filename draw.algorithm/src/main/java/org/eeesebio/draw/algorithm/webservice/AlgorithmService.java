package org.eeesebio.draw.algorithm.webservice;

import java.util.Collection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eeesebio.draw.algorithm.Draw;
import org.eeesebio.draw.algorithm.model.DrawResult;
import org.eeesebio.draw.algorithm.model.GenerateDrawInput;
import org.eeesebio.draw.algorithm.model.NotAllowedConstraint;
import org.jacop.core.Domain;
import org.jacop.core.SmallDenseDomain;

@Path("/")
public class AlgorithmService {

	@GET
	@Path("/hi")
	@Produces(MediaType.TEXT_PLAIN)
	public String sayHello() {
		System.out.println("answer to 'hi' request");
		return "hello world";
	}
	
	@POST
	@Path("/generate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public DrawResult generateDraw(GenerateDrawInput input){
		Collection<NotAllowedConstraint> constraints = input.getConstraints();
		
		System.out.println("answer to 'generate' request");

		Draw draw = new Draw(input.getSize(), constraints);
		Domain[] solution = draw.getSolution();
		
		DrawResult drawResult = new DrawResult();
		for (int j = 0; j < solution.length; j++) {
			drawResult.getParticipants().put(Integer.toString(j), Integer.toString(((SmallDenseDomain) solution[j]).min));
		}
		
		return drawResult;
	}
}
